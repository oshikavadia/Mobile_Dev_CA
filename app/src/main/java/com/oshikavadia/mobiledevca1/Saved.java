package com.oshikavadia.mobiledevca1;

import java.io.Serializable;
import java.sql.Blob;
import java.util.HashMap;

/**
 * Created by Shane on 05/12/2017.
 */

public class Saved implements Serializable {
    private int _id;
    private String saveName;
    private String urlName;
    private HashMap<String, String> content;

    public Saved(int _id, String saveName, String urlName, HashMap<String, String> content) {
        this._id = _id;
        this.saveName = saveName;
        this.urlName = urlName;
        this.content = content;
    }

    public Saved(int i, HashMap<String, String> hm) {
        this._id = i;
        this.content = hm;
    }

    public Saved(String name, HashMap<String, String> hm) {
        this.saveName = name;
        this.content = hm;
    }

    public Saved() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getSaveName() {
        return saveName;
    }

    public void setSaveName(String saveName) {
        this.saveName = saveName;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Saved that = (Saved) o;

        if (_id != that._id) return false;
        if (saveName != null ? !saveName.equals(that.saveName) : that.saveName != null)
            return false;
        if (urlName != null ? !urlName.equals(that.urlName) : that.urlName != null) return false;
        return content != null ? content.equals(that.content) : that.content == null;
    }

    @Override
    public int hashCode() {
        int result = _id;
        result = 31 * result + (saveName != null ? saveName.hashCode() : 0);
        result = 31 * result + (urlName != null ? urlName.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Saved{" +
                "_id=" + _id +
                ", saveName='" + saveName + '\'' +
                ", urlName='" + urlName + '\'' +
                ", content=" + content +
                '}';
    }
}
