package com.oshikavadia.mobiledevca1;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SearchHistory extends AppCompatActivity {
    private DatabaseHelper db = new DatabaseHelper(this);
    private ArrayAdapter adapter;
    private List<String> searchHistoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_history);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        searchHistoryList = getHistoryArticles();
        ListView lv = (ListView) findViewById(R.id.search_history_list);
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, searchHistoryList);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            String title = searchHistoryList.get(position);
            Intent i = new Intent(this, article_content.class);
            i.putExtra("article", title);
            startActivity(i);
        });
    }

    public void clearSearchHistory(View view) {
        db.deleteAllSearches();
        searchHistoryList.clear();
        searchHistoryList.addAll(getHistoryArticles());
        adapter.notifyDataSetChanged();
    }

    private List<String> getHistoryArticles() {
        List<Search> searches = db.getAllSearchs();
        List<String> names = new ArrayList<>();
        for (Search search : searches) {

            names.add(search.getSearchKey());
        }
        return names;
    }
}
