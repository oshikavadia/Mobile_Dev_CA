package com.oshikavadia.mobiledevca1;

/**
 * Created by Shane on 02/12/2017.
 */

public class Search
{
    int _id;
    String searchKey;
    String date;
//    Maybe use Text data type for 'date'

    public Search(int _id, String searchKey, String date) {
        this._id = _id;
        this.searchKey = searchKey;
        this.date = date;
    }

    public Search(String searchKey) {
        this._id = 0;
        this.searchKey = searchKey;
        this.date = "";
    }
    public Search()
    {

    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Search{" +
                "_id=" + _id +
                ", searchKey='" + searchKey + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Search search = (Search) o;

        if (_id != search._id) return false;
        if (searchKey != null ? !searchKey.equals(search.searchKey) : search.searchKey != null)
            return false;
        return date != null ? date.equals(search.date) : search.date == null;
    }

    @Override
    public int hashCode() {
        int result = _id;
        result = 31 * result + (searchKey != null ? searchKey.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
