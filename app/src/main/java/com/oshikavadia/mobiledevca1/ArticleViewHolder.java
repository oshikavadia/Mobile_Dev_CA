package com.oshikavadia.mobiledevca1;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Oshi on 07-Nov-17.
 */

public class ArticleViewHolder {
    TextView name = null;
    ImageView image = null;

    ArticleViewHolder(View artView) {
        this.name = (TextView) artView.findViewById(R.id.articleTitle);
        this.image = (ImageView) artView.findViewById(R.id.articleImage);
    }
}
