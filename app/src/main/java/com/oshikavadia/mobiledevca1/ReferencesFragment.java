package com.oshikavadia.mobiledevca1;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReferencesFragment extends Fragment {
    private static List<String> refs = new ArrayList<>();
    private View view;
    private ArrayAdapter adapter;
    private static final String FRAGMENT_NAME = "name";
    private static final String FRAGMENT_NUMBER = "number";

    private String name;
    private int number;

    private OnFragmentInteractionListener mListener;

    public ReferencesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param name     Name of the fragment screen.
     * @param position position of the fragment tab.
     * @return A new instance of fragment ContentFragment.
     */
    public static ReferencesFragment newInstance(String name, int position) {
        ReferencesFragment fragment = new ReferencesFragment();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_NAME, name);
        args.putInt(FRAGMENT_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(FRAGMENT_NAME);
            number = getArguments().getInt(FRAGMENT_NUMBER);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_references, container, false);
        ListView lv = (ListView) view.findViewById(R.id.refList);
        adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, refs);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, v, position, id) -> {
            if (position < refs.size()) {
//                String url = refs.get(position);
//                Log.i("Url", url);
//                Intent i = new Intent(getActivity(), webView.class);
//                i.putExtra("url", url);
//                startActivity(i);
                String url = refs.get(position);
                if (url.startsWith("//")) {
                    url = url.replaceFirst("//", "http://");
                }
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(getActivity(), Uri.parse(url));
            }

        });
        String title = ((article_content) getActivity()).getArticleTitle();
        new getArticleLists().execute(title);
        return view;
    }

    public class getArticleLists extends AsyncTask<String, Void, List<String>> {

        @Override
        protected List<String> doInBackground(String... strings) {
            String title = strings[0];
            List<String> links = new ArrayList<>();
            try {
                String json = Utils.getResource(String.format(Constants.WikipediaGetExternalLinks, title));
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                }
                JSONArray linksArray = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getJSONArray("extlinks");
                for (int i = 0; i < linksArray.length(); i++) {
                    links.add(linksArray.getJSONObject(i).getString("*"));
                }
            } catch (Exception e) {
                Log.e("Get reference", e.getMessage());
                e.printStackTrace();
            }
            return links;
        }

        @Override
        protected void onPreExecute() {
            view.findViewById(R.id.reference_progress_bar).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<String> results) {
            view.findViewById(R.id.reference_progress_bar).setVisibility(View.GONE);
            refs.addAll(results);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        refs.clear();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
