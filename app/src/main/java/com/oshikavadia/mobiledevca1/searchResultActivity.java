package com.oshikavadia.mobiledevca1;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class searchResultActivity extends baseActivity {
    private TextView tv;
    private ArrayList<ArticleMetadata> articles = new ArrayList<>();
    private ListView lv;
    private ArrayAdapter<ArticleMetadata> adapter;
    private DatabaseHelper db = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        tv = findViewById(R.id.search_text);
        tv.setText("Search test");
        if (articles.size() == 0) {
            lv = (ListView) findViewById(R.id.search_list_view);

            adapter = new ArticleAdapter(searchResultActivity.this, R.layout.article_row_layout, R.id.articleTitle, articles);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener((parent, view, position, id) -> {
                Toast.makeText(searchResultActivity.this, "Open", Toast.LENGTH_SHORT).show();
                Log.d("click", articles.get(position).getName());
                Intent i = new Intent(searchResultActivity.this, article_content.class);
                i.putExtra("article", articles.get(position).getName());
                startActivity(i);
            });


        }
        String query = "";
        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            query = getIntent().getStringExtra(SearchManager.QUERY);
            Log.i("Search Class", query);
            tv.setText(query);
            Search search = new Search(query);
            db.addSearch(search);
            new searchArticles().execute(query);
        }

    }

    public class searchArticles extends AsyncTask<String, Void, List<ArticleMetadata>> {

        @Override
        protected List<ArticleMetadata> doInBackground(String... strings) {
            List<ArticleMetadata> returnList = new ArrayList<>();
            String query = strings[0];
            String json = Utils.getResource(String.format(Constants.WikipediaSearch, query));
            try {
                JSONArray root = new JSONArray(json);
                JSONArray title = root.getJSONArray(1);
                for (int i = 0; i < title.length(); i++) {
                    Log.i("Search", title.getString(i));
                    returnList.add(getArticle(title.getString(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return returnList;
        }

        private ArticleMetadata getArticle(String title) {

            String url = "";

            String NormalisedTitle = Utils.normaliseWikiTitle(title);
            Log.i("title", title);


            try {
                String json = Utils.getResource(String.format(Constants.WikipediaGetArticleMainImage, NormalisedTitle));
                if (json == null) return null;
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                    Log.d("Image JSON", json);
                    Bitmap imgBitmap;
                    if (json != null && jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).has("thumbnail")) {
                        String imgSrc = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getJSONObject("thumbnail").getString("source");
                        imgBitmap = Utils.getBitmapFromURL(imgSrc);
                    } else {
                        imgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
                    }
                    ArticleMetadata temp = new ArticleMetadata(title, "",
                            Constants.WikipediaGetArticle + NormalisedTitle, imgBitmap);
                    return temp;
                }
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;

            }
        }

        @Override
        protected void onPostExecute(List<ArticleMetadata> results) {
            findViewById(R.id.search_spinner).setVisibility(View.GONE);
            articles.addAll(results);
            Log.d("Article size", articles.size() + "");
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            findViewById(R.id.search_spinner).setVisibility(View.VISIBLE);
        }
    }


}

