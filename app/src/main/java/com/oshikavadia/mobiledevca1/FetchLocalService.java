package com.oshikavadia.mobiledevca1;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
 * Created by Oshi on 01-Mar-18.
 */

public class FetchLocalService extends Service {
    private String TAG = "FetchLocalService";
    private NotificationManager mNM;
    private int NOTIFICATION = 1;
    private ArrayList<ArticleMetadata> articles = new ArrayList<>();
    private long lastUpdateTime;
    private final IBinder mBinder = new LocalBinder();
    private List<Tags> tags = new ArrayList<Tags>();
    private DatabaseHelper db = new DatabaseHelper(this);

    public FetchLocalService() {
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        FetchLocalService getService() {
            return FetchLocalService.this;
        }
    }

    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public ArrayList<ArticleMetadata> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<ArticleMetadata> articles) {
        this.articles = articles;
    }


    public class getArticles extends AsyncTask<Integer, Void, List<ArticleMetadata>> {


        @Override
        protected List<ArticleMetadata> doInBackground(Integer... ints) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            int numOfArticle = ints[0];
            ArrayList<ArticleMetadata> return_list = new ArrayList<>();
            List<Thread> threadList = new ArrayList<>();
            for (int i = 0; i < numOfArticle; i++) {

                Thread t = new Thread(() -> {
                    Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                    ArticleMetadata temp = getSingleArticle();
                    if (temp != null) {
                        return_list.add(temp);
                    }
                });
                threadList.add(t);
                t.run();
                publishProgress();
            }
            for (Thread t : threadList) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return return_list;
        }

        private ArticleMetadata getSingleArticle() {
            String json = Utils.getResource(Constants.WikipediaGetRandomArticle);
            while (json == null) json = Utils.getResource(Constants.WikipediaGetRandomArticle);
            json.trim();

            String NormalisedTitle = "", title = "", url = "";
            try {
                Log.d(TAG, json);
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                }
                title = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getString("title");
                NormalisedTitle = Utils.normaliseWikiTitle(title);
                Log.i(TAG, title);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                json = Utils.getResource(String.format(Constants.WikipediaGetArticleMainImage, NormalisedTitle));
                if (json == null) return null;
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                    Log.d(TAG, json);
                    Bitmap imgBitmap;
                    if (json != null && jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).has("thumbnail")) {
                        String imgSrc = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getJSONObject("thumbnail").getString("source");
                        imgBitmap = Utils.getBitmapFromURL(imgSrc);
                    } else {
                        imgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
                    }
                    ArticleMetadata temp = new ArticleMetadata(title, "",
                            Constants.WikipediaGetArticle + NormalisedTitle, imgBitmap);
                    return temp;
                }
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;

            }


        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        stopForeground(true);
        return mBinder;
    }

    @Override
    public void onCreate() {
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        final long period = 5 * 60 * 1000; //in milliseconds
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Data fetch");
                tags.clear();
                tags.addAll(db.getAllTags());
                for (int i = 0; i < 10; i++) {
                    List<ArticleMetadata> newArticles = new ArrayList<>();
                    try {
                        newArticles = new getArticles().execute(1).get();
                        for (ArticleMetadata am : newArticles) {
                            for (Tags tag : tags) {
                                if (tag.getTag().equalsIgnoreCase(am.getName()) || tag.getTag().equalsIgnoreCase("*")) {
                                    Intent showTaskIntent = new Intent(getApplicationContext(), article_content.class);
                                    showTaskIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP

                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    // The PendingIntent to launch our activity if the user selects this notification
                                    showTaskIntent.putExtra("article", am.getName());
                                    PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, showTaskIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                                    // Set the info for the views that show in the notification panel.
                                    Notification notification = null;
                                    CharSequence text = "We found an article that might interest you.";
                                    CharSequence title = am.getName();
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        notification = new Notification.Builder(getApplicationContext())
                                                .setContentTitle(title)
                                                .setContentText(text)
                                                .setSmallIcon(R.drawable.ic_public_black_24dp)
                                                .setChannelId("channel_all")
                                                .setContentIntent(contentIntent)
                                                .setWhen(System.currentTimeMillis())
                                                .setLargeIcon(am.getImage())
                                                .build();
                                    } else {
                                        notification = new Notification.Builder(getApplicationContext())
                                                .setContentTitle(title)
                                                .setContentText(text)
                                                .setSmallIcon(R.drawable.ic_public_black_24dp)
                                                .setContentIntent(contentIntent)
                                                .setWhen(System.currentTimeMillis())
                                                .setLargeIcon(am.getImage())
                                                .build();
                                    }
                                    NotificationChannel channel = null;
                                    notification.flags |= Notification.FLAG_AUTO_CANCEL;
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        channel = new NotificationChannel("channel_all", "All Channel", NotificationManager.IMPORTANCE_HIGH);
                                        mNM.createNotificationChannel(channel);
                                    }
                                    mNM.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notification);
                                }
                            }
                        }
                        articles.addAll(newArticles);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                lastUpdateTime = System.currentTimeMillis() / 1000L;
            }
        }, 0, period);
        mNM.notify(NOTIFICATION, getNotification());
        // Display a notification about us starting.  We put an icon in the status bar.
//        showNotification();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);
//        startForeground(NOTIFICATION, getNotification());
        return START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
//        startForeground(NOTIFICATION, getNotification());
        Toast.makeText(this, "local_service_unbind", Toast.LENGTH_SHORT).show();

        return true;
    }

    @Override
    public void onDestroy() {
        mNM.cancel(NOTIFICATION);

        // Tell the user we stopped.
        Toast.makeText(this, "local_service_stopped", Toast.LENGTH_SHORT).show();
    }


    private Notification getNotification() {
        CharSequence text = "Service is running in the background.";

        Intent showTaskIntent = new Intent(getApplicationContext(), MainActivity.class);
        // The PendingIntent to launch our activity if the user selects this notification
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, showTaskIntent, 0);

        // Set the info for the views that show in the notification panel.
        Notification notification = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(getApplicationContext())
                    .setContentTitle("Local Bound Service Started")
                    .setContentText(text)
                    .setSmallIcon(R.drawable.ic_sync_black_24dp)
                    .setChannelId("channel_all")
                    .setContentIntent(contentIntent)
                    .setWhen(System.currentTimeMillis())
                    .build();
        } else {
            notification = new Notification.Builder(getApplicationContext())
                    .setContentTitle("Local Bound Service Started")
                    .setContentText(text)
                    .setSmallIcon(R.drawable.ic_sync_black_24dp)
                    .setContentIntent(contentIntent)
                    .setWhen(System.currentTimeMillis())
                    .build();
        }
        NotificationChannel channel = null;
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel("channel_all", "All Channel", NotificationManager.IMPORTANCE_HIGH);
            mNM.createNotificationChannel(channel);
        }

        return notification;
    }
}
