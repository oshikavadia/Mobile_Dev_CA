package com.oshikavadia.mobiledevca1;

/**
 * Created by Oshi on 27-Nov-17.
 */

public class Constants {
    static public String WikipediaGetRandomArticle = "https://en.wikipedia.org/w/api.php?action=query&generator=random&grnnamespace=0&prop=extracts&exchars=500&format=json";
    static public String WikipediaGetDataBySection = "https://en.wikipedia.org/w/api.php?action=parse&page=%s&format=json&prop=text&section=%d" +
            "&disableeditsection=true&disablelimitreport=true&disabletoc=true&sectionpreview=true";
    static public String WikipediaGetSectionsForArticle = "https://en.wikipedia.org/w/api.php?action=parse&format=json&page=%s&prop=sections";
    static public String WikipediaGetExtractForArticle = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=%s";
    static public String WikipediaGetArticle = "https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=";
    static public String WikipediaGetArticleMainImage = "https://en.wikipedia.org/w/api.php?action=query&titles=%s&prop=pageimages&format=json&pithumbsize=500";
    static public String WikipediaGetArticleImages = "https://en.wikipedia.org/w/api.php?action=query&prop=images&format=json&titles=%s";
    static public String WikipediaGetImageURL = "https://commons.wikimedia.org/wiki/Special:FilePath/%s";
    static public String WikipediaGetImageThumbNailURL = "https://commons.wikimedia.org/wiki/Special:FilePath/%s?width=200";
    static public String WikipediaSearch = "https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=%s";
    static public String WikipediaGetExternalLinks = "https://en.wikipedia.org/w/api.php?action=query&titles=%s&prop=extlinks&format=json";
}
