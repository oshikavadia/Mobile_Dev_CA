package com.oshikavadia.mobiledevca1;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class imageViewActivity extends baseActivity {
    private PhotoView photoView;
    private Bitmap bitmap;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        photoView = (PhotoView) findViewById(R.id.photo_view);
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        String url = intent.getStringExtra("url");
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(name);
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        new getImage().execute(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void downloadImage(View view) {
        Toast.makeText(this,
                "saving image", Toast.LENGTH_SHORT).show();
        new saveImage().execute();
    }

    public class saveImage extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean success = false;
            if (bitmap != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(imageViewActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                }
                File sdCardDirectory = Environment.getExternalStorageDirectory();
                File image = new File(sdCardDirectory, getString(R.string.folderName) + File.separator + name);
                success = false;
                //Encode the file as a PNG image.
                FileOutputStream outStream;
                try {

                    outStream = new FileOutputStream(image);
                    String fmt = name.substring(name.lastIndexOf('.') + 1).toUpperCase();
                    Bitmap.CompressFormat format = (fmt.equals("JPG") || fmt.equals("JPEG")) ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.PNG;
                    bitmap.compress(format, 100, outStream);
                    outStream.flush();
                    outStream.close();
                    success = true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                Toast.makeText(getApplicationContext(), "Image saved!",
                        Toast.LENGTH_LONG).show();
                Notification notif = null;
                Uri photoURI = FileProvider.getUriForFile(imageViewActivity.this, getApplicationContext().getPackageName() + ".provider", new File(Environment.getExternalStorageDirectory(), getString(R.string.folderName) + File.separator + name));
                Intent intent = new Intent(Intent.ACTION_VIEW, photoURI);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                        intent, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notif = new Notification.Builder(getApplicationContext())
                            .setContentTitle("Image Downloaded")
                            .setContentText("Saved Image " + name)
                            .setSmallIcon(R.drawable.ic_file_download_black_24dp)
                            .setLargeIcon(bitmap)
                            .setStyle(new Notification.BigPictureStyle()
                                    .bigPicture(bitmap))
                            .setChannelId("channel_all")
                            .setContentIntent(contentIntent)
                            .build();
                } else {
                    notif = new Notification.Builder(getApplicationContext())
                            .setContentTitle("Image Downloaded")
                            .setContentText("Saved Image " + name)
                            .setSmallIcon(R.drawable.ic_file_download_black_24dp)
                            .setLargeIcon(bitmap)
                            .setStyle(new Notification.BigPictureStyle()
                                    .bigPicture(bitmap))
                            .setContentIntent(contentIntent)
                            .build();
                }


                notif.flags |= Notification.FLAG_AUTO_CANCEL;
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel channel = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    channel = new NotificationChannel("channel_all", "All Channel", NotificationManager.IMPORTANCE_HIGH);
                    mNotificationManager.createNotificationChannel(channel);
                }
                mNotificationManager.notify(0, notif);


            } else {
                Toast.makeText(getApplicationContext(),
                        "Error saving image!", Toast.LENGTH_LONG).show();
            }
        }

    }

    public class getImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            return Utils.getBitmapFromURL(strings[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bmp) {
            findViewById(R.id.image_progress_bar).setVisibility(View.GONE);
            photoView.setImageBitmap(bmp);
            bitmap = bmp;
        }

        @Override
        protected void onPreExecute() {
            findViewById(R.id.image_progress_bar).setVisibility(View.VISIBLE);
        }
    }
}
