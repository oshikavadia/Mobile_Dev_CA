package com.oshikavadia.mobiledevca1;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Oshi on 12-Nov-17.
 */

public class MediaViewHolder {
    TextView name = null;
    ImageView image = null;

    MediaViewHolder(View artView) {
        this.name = (TextView) artView.findViewById(R.id.media_text_view);
        this.image = (ImageView) artView.findViewById(R.id.media_image_view);
    }
}
