package com.oshikavadia.mobiledevca1;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class MainActivity extends baseActivity {

    private ArrayList<ArticleMetadata> articles = new ArrayList<>();
    private ListView lv;
    private ArrayAdapter<ArticleMetadata> adapter;
    private final int articleCount = 10;
    private DatabaseHelper db = new DatabaseHelper(this);
    FetchLocalService myService;
    boolean isBound = false;

    private ServiceConnection myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            FetchLocalService.LocalBinder binder = (FetchLocalService.LocalBinder) service;
            myService = binder.getService();
            isBound = true;
            runOnUiThread(() -> {
                updateArticles();
            });

        }

        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();


//        if (articles.size() == 0) {


        Intent intent = new Intent(this, FetchLocalService.class);
        bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
        Log.d("onCreate", "Bound");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("onResume", "Bound");
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound) {
            unbindService(myConnection);
            isBound = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Log.d("onCreate", "No SDCARD");
        } else {
            File directory = new File(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.folderName));
            directory.mkdirs();
        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        lv = (ListView) findViewById(R.id.featuredArticleList);

        adapter = new ArticleAdapter(MainActivity.this, R.layout.article_row_layout, R.id.articleTitle, articles);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Toast.makeText(MainActivity.this, "Open", Toast.LENGTH_SHORT).show();
            Log.d("click", articles.get(position).getName());
            Intent i = new Intent(MainActivity.this, article_content.class);
            i.putExtra("article", articles.get(position).getName());
            startActivity(i);
        });
//        }
        SwipeRefreshLayout srl = (SwipeRefreshLayout) (findViewById(R.id.swiperefresh));
        srl.setOnRefreshListener(
                () -> {
                    Log.i("Main Activity", "onRefresh called from SwipeRefreshLayout");

                    articles.clear();
                    new getArticles().execute(10);

                    srl.setRefreshing(false);
                }
        );
    }

    public void saveOnClick(View v) {
        Toast.makeText(this, "Saving", Toast.LENGTH_SHORT).show();
        Log.d("Save Onclick", "click");
        String name = ((ArticleMetadata) v.getTag()).getName();
        new getDataToSave().execute(name);

    }

    public class getDataToSave extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            HashMap<String, String> sectionData = new HashMap<>();
            String name = strings[0];
            try {
                String json = Utils.getResource(String.format(Constants.WikipediaGetExtractForArticle, Utils.normaliseWikiTitle(name)));
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                }
                String extract = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getString("extract");
                sectionData.put("Extract", extract);
                json = Utils.getResource(String.format(Constants.WikipediaGetSectionsForArticle, Utils.normaliseWikiTitle(name)));

                jObject = new JSONObject(json);
                JSONArray sections = jObject.getJSONObject("parse").getJSONArray("sections");
                for (int i = 0; i < sections.length(); i++) {
                    JSONObject jsonObj = sections.getJSONObject(i);
                    String section = jsonObj.getString("line");

                    int index = jsonObj.getInt("index");
                    @SuppressLint("DefaultLocale") String data = new JSONObject(Utils.getResource(
                            String.format(Constants.WikipediaGetDataBySection, name, index))).getJSONObject("parse").getJSONObject("text").getString("*");
                    data = Utils.cleanHtml(data);
                    Log.i("Section data", section);

                    sectionData.put(section, data);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            Saved saved = new Saved(name, sectionData);
            db.addSavedArticle(saved);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
        }

    }

    public void pinOnClick(View v) {
        String name = ((ArticleMetadata) v.getTag()).getName();
        Toast.makeText(this, "Pinned " + name, Toast.LENGTH_SHORT).show();
        Pinned article = new Pinned(name);
        db.addPinArticle(article);
        Log.d("pin Onclick", "click");
    }

    public void starOnClick(View v) {
        Toast.makeText(this, "Starred \"" + ((ArticleMetadata) v.getTag()).getName() + "\"", Toast.LENGTH_SHORT).show();
        Log.i("Star onclick", "" + v.getTag());
        Log.d("star Onclick", "click");
    }

    public void updateArticles() {

        new getArticles().execute(10);


    }

    public class setArticlesAsync extends AsyncTask<List<ArticleMetadata>, Void, List<ArticleMetadata>> {

        @Override
        protected List<ArticleMetadata> doInBackground(List<ArticleMetadata>... list) {
            return list[0];
        }

        @Override
        protected void onPostExecute(List<ArticleMetadata> results) {
            articles.addAll(results);
            Log.d("Article size", articles.size() + "");
            adapter.notifyDataSetChanged();
        }
    }

    public class getArticles extends AsyncTask<Integer, ArticleMetadata, List<ArticleMetadata>> {


        @Override
        protected List<ArticleMetadata> doInBackground(Integer... ints) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            int numOfArticle = ints[0];
            ArrayList<ArticleMetadata> return_list = new ArrayList<>();
            if (isBound) {
                List<ArticleMetadata> listFromService = myService.getArticles();
                if (listFromService.size() > 0) {
                    for (ArticleMetadata a : listFromService) {
                        publishProgress(a);
                    }
                    numOfArticle = numOfArticle - listFromService.size();
                }
            }
            for (int i = 0; i < numOfArticle; i++) {
                ArticleMetadata temp;

                temp = getSingleArticle();
                if (temp != null) {
                    return_list.add(temp);
                }


                publishProgress(temp);
            }
            return return_list;
        }

        private ArticleMetadata getSingleArticle() {
            String json = Utils.getResource(Constants.WikipediaGetRandomArticle);
            while (json == null) json = Utils.getResource(Constants.WikipediaGetRandomArticle);
            json.trim();

            String NormalisedTitle = "", title = "", url = "";
            try {
                Log.d("JSON", json);
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                }
                title = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getString("title");
                NormalisedTitle = Utils.normaliseWikiTitle(title);
                Log.i("title", title);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                json = Utils.getResource(String.format(Constants.WikipediaGetArticleMainImage, NormalisedTitle));
                if (json == null) return null;
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                    Log.d("Image JSON", json);
                    Bitmap imgBitmap;
                    if (json != null && jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).has("thumbnail")) {
                        String imgSrc = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getJSONObject("thumbnail").getString("source");
                        imgBitmap = Utils.getBitmapFromURL(imgSrc);
                    } else {
                        imgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
                    }
                    ArticleMetadata temp = new ArticleMetadata(title, "",
                            Constants.WikipediaGetArticle + NormalisedTitle, imgBitmap);
                    return temp;
                }
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;

            }
        }

        @Override
        protected void onPostExecute(List<ArticleMetadata> results) {

        }

        @Override
        protected void onPreExecute() {
            findViewById(R.id.articleProgressBar).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(ArticleMetadata... values) {
            findViewById(R.id.articleProgressBar).setVisibility(View.GONE);
            articles.add(values[0]);
            Log.d("Article size", articles.size() + "");
            adapter.notifyDataSetChanged();
        }
    }

}
