package com.oshikavadia.mobiledevca1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MediaFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String FRAGMENT_NAME = "name";
    private static final String FRAGMENT_NUMBER = "number";
    private static ArrayList<MediaMetadata> images = new ArrayList<>();
    GridView gridView;
    private String name;
    private int number;
    private String title;
    private OnFragmentInteractionListener mListener;
    private LayoutInflater inflater;
    private ViewGroup container;
    private View view;
    private ArrayAdapter<MediaMetadata> adapter;
    public MediaFragment() {
        // Required empty public constructor
    }

    public static MediaFragment newInstance(String name, int position) {
        MediaFragment fragment = new MediaFragment();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_NAME, name);
        args.putInt(FRAGMENT_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_media, container, false);
        this.inflater = inflater;
        this.container = container;
        if (getArguments() != null) {
            name = getArguments().getString(FRAGMENT_NAME);
            number = getArguments().getInt(FRAGMENT_NUMBER);
        }
//        for (int i = 0; i < 10; i++) {
//            images.add(new MediaMetadata("The Parthenon", "", BitmapFactory.decodeResource(getResources(),
//                    R.drawable.parthenon)));
//        }
        images.clear();
        gridView = (GridView) view.findViewById(R.id.media_grid);
        adapter = new ImageAdapter(getContext(), R.layout.image_media, R.id.media_text_view, images);
        gridView.setAdapter(adapter);
        title = ((article_content) getActivity()).getArticleTitle();
        new getArticleImages().execute();
        gridView.setOnItemClickListener((parent, v, position, id) -> {
            MediaMetadata metadata = images.get(position);
            Intent i = new Intent(getContext(), imageViewActivity.class);
            i.putExtra("url", metadata.getUrl());
            i.putExtra("name", metadata.getName());
            startActivity(i);
        });
        return view;
    }

    public class getArticleImages extends AsyncTask<Void, Void, List<MediaMetadata>> {

        @Override
        protected List<MediaMetadata> doInBackground(Void... params) {
            List<MediaMetadata> mediaData = new ArrayList<>();

            try {
                String json = Utils.getResource(String.format(Constants.WikipediaGetArticleImages, title));
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                }

                JSONArray sections = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getJSONArray("images");
                for (int i = 0; i < sections.length(); i++) {
                    JSONObject jsonObj = sections.getJSONObject(i);
                    String name = jsonObj.getString("title");
                    String imageName = name.replaceAll("File:", "");
                    imageName = Utils.normaliseWikiTitle(imageName);
                    String ThumbURL = String.format(Constants.WikipediaGetImageThumbNailURL, imageName);
                    String imageURL = String.format(Constants.WikipediaGetImageURL, imageName);
                    if (!name.contains("svg")) {
                        Bitmap image = Utils.getBitmapFromURL(ThumbURL);
                        MediaMetadata metadata = new MediaMetadata(imageName, imageURL, image);
                        mediaData.add(metadata);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mediaData;
        }

        @Override
        protected void onPreExecute() {
            view.findViewById(R.id.content_progress_bar).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<MediaMetadata> results) {
            view.findViewById(R.id.content_progress_bar).setVisibility(View.GONE);
            images.addAll(results);
            adapter.notifyDataSetChanged();
        }

    }
    @Override
    public void onDetach() {
        super.onDetach();
        images.clear();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
