package com.oshikavadia.mobiledevca1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

public class savedArticles extends baseActivity {
    ListView lv;
    ArrayAdapter adapter;
    List<Saved> savedArticles;
    DatabaseHelper db = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_articles);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.savedArticles = db.getAllSavedArticles();
        lv = findViewById(R.id.savedArticleList);
        adapter = new savedAdapter(this, R.layout.saved_list_item, R.id.savedArticleName, savedArticles);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Saved s = savedArticles.get(position);
            Intent i = new Intent(this, SavedArticleContent.class);
            i.putExtra("saved", s);
            startActivity(i);
        });
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public class savedAdapter extends ArrayAdapter<Saved> {
        List<Saved> savedList;
        LayoutInflater inflater;

        public savedAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Saved> objects) {
            super(context, resource, textViewResourceId, objects);
            this.savedList = objects;
            inflater = getLayoutInflater();
        }

        public int getCount() {
            return savedList.size();
        }

        public Saved getItem(Saved position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView display_name;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            try {
                if (convertView == null) {
                    vi = inflater.inflate(R.layout.saved_list_item, null);
                    holder = new ViewHolder();

                    holder.display_name = (TextView) vi.findViewById(R.id.savedArticleName);


                    vi.setTag(holder);
                } else {
                    holder = (ViewHolder) vi.getTag();
                }


                holder.display_name.setText(savedList.get(position).getSaveName());


            } catch (Exception e) {


            }
            return vi;
        }
    }

}
